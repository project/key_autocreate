<?php

namespace Drupal\key_autocreate;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\key\Entity\Key;
use Drupal\key\Entity\KeyConfigOverride;

/**
 * Helper service for key_autocreate module.
 */
class Helper {

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $extensionListModule;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a Helper object.
   *
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ModuleExtensionList $extension_list_module, ConfigFactoryInterface $config_factory) {
    $this->extensionListModule = $extension_list_module;
    $this->configFactory = $config_factory;
  }

  /**
   * Returns a list of supported module extensions that are being installed.
   *
   * @param array<int,string> $modules
   *   The list of modules being installed.
   *
   * @return array<string,array<string,mixed>
   *   An array of supported module extensions.
   */
  public function getSupportedModules(array $modules): array {

    $module_data = $this->extensionListModule->reset()->getList();
    $modules_installed = array_intersect_key($module_data, array_flip($modules));
    $supported_modules_installed = [];

    foreach ($modules_installed as $machine_name => $module) {
      // Support all modules that have keys in their schema.
      if ($schema_file_contents = file_get_contents(DRUPAL_ROOT . '/' . $module->subpath . "/config/schema/$machine_name.schema.yml")) {
        $schema = Yaml::decode($schema_file_contents);
        foreach ($schema as $setting_key => $settings) {
          // No support for multi settings?
          if (strpos($setting_key, '*') === FALSE && isset($settings['mapping'])) {
            $mapping = array_filter($settings['mapping'], function ($key) {
              return preg_match('[app_id|app_secret|client_id|client_secret|api_key|apiKey|clientId|privateKey]', $key);
            }, ARRAY_FILTER_USE_KEY);
            if ($mapping) {
              // Not sure if module is needed yet.
              $supported_modules_installed[$machine_name]['module'] = $module;
              $supported_modules_installed[$machine_name]['settings'][$setting_key] = $settings;
              // Override mapping with needed mapping. Can probably clean this
              // up to just alter the $settings array.?
              $supported_modules_installed[$machine_name]['settings'][$setting_key]['mapping'] = $mapping;
            }
          }
        }
      }
    }

    return $supported_modules_installed;
  }

  /**
   * Creates the keys and config overrides for the modules being enabled.
   *
   * @param array<string,array<string,mixed> $supported_modules
   *   The supported module.
   */
  public function createKeyAndKeyConfigOverride($supported_modules) {
    foreach ($supported_modules as $supported_module) {
      foreach ($supported_module['settings'] as $setting_id => $settings) {
        $settingsConfig = $this->configFactory->getEditable($setting_id);
        foreach ($settings['mapping'] as $map_id => $map) {
          // Save null value in key setting.
          $settingsConfig->set($map_id, NULL)->save();
          $id = strtolower(str_replace('.', '__', $setting_id . '__' . $map_id));
          $label = $settings['label'] . ': ' . $map['label'];
          $keyConfigExists = $this->configFactory->get("key.key.$id")->get('id');
          // Create key if it does not exist yet.
          if (!$keyConfigExists) {
            $key = Key::create([
              'id' => $id,
              'label' => $label,
              'description' => '',
              'key_type' => 'authentication',
              'key_type_settings' => [],
              // @todo make this setting configurable.
              'key_provider' => 'env',
              'key_provider_settings' => [
                'env_variable'  => strtoupper($id),
                'strip_line_breaks' => TRUE,
              ],
              'key_input' => 'none',
              'key_input_settings' => [],
            ])->save();
          }
          // Create config override if key exists and override not yet.
          // This currently is only possible for system.simple. Need to find a
          // way to do this properly for multiple configuration types such as
          // used in geofield_map and geocoder.
          if ($settings['type'] === 'config_object'
          && ($keyConfigExists || $key)
          && !$this->configFactory->get("key.config_override.$id")->get('id')) {
            KeyConfigOverride::create([
              'id' => $id,
              'label' => $label,
              'config_type' => $settings['type'] !== 'config_object' ?: 'system.simple',
              'config_prefix' => '',
              'config_name' => $setting_id,
              'config_item' => $map_id,
              'key_id' => $id,
            ])->save();
          }
        }
      }
    }
  }

  /**
   * Deletes the keys and config overrides for the module being uninstalled.
   *
   * @param array<string,array<string,mixed> $supported_modules
   *   The supported modules.
   */
  public function deleteKeyAndKeyConfigOverride($supported_modules) {
    foreach ($supported_modules as $supported_module) {
      foreach ($supported_module['settings'] as $setting_id => $settings) {
        foreach ($settings['mapping'] as $map_id => $map) {
          $id = strtolower(str_replace('.', '__', $setting_id . '__' . $map_id));
          $this->configFactory->getEditable("key.key.$id")->delete();
          $this->configFactory->getEditable("key.config_override.$id")->delete();
        }
      }
    }
  }

}
